let res = [];
function flatteningNestedArray(array) {
    for (let index = 0; index < array.length; index++) {
        if (Array.isArray(array[index])) {
            flatteningNestedArray(array[index]);
        }
        else {
            res.push(array[index]);
        }
    }
    return res;
}
module.exports = flatteningNestedArray;