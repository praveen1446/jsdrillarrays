function sampleForEach(array,callback){
    for(let current=0;current<array.length;current++){
        callback(array,current);
    }
}
module.exports=sampleForEach;