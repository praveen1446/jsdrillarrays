function sampleFilter(array,callback){
    if(Array.isArray(array)){
        let newArray=[];
        for(let current=0;current<array.length;current++){
            if(callback(array[current])){
                newArray.push(array[current]);
            }
        }
        if(newArray.length>0){
            return newArray;
        }
        else{
            return [];
        }
    }
    else{
        return [];
    }
}
module.exports=sampleFilter;
