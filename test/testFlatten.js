const flatteningNestedArray = require('./flatten.js');
const nestedArray = [1, [2], [[3]], [[[4]]]];
const flattenArray = flatteningNestedArray(nestedArray);

console.log(`The required array after flattening the original nested array ${nestedArray} is: ${flattenArray}`);