const find=require('../find.js');
var array = [1, 2, 3, 4, 5, 5];
var initialCount = 0;

function callBck(array, index) {
    const result = array[index];
    initialCount = initialCount + result;
}
find(array, callBck);

if (initialCount === 20) {
    console.log("true");
} else {
    console.log("undefined..");
}