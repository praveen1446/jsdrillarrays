const sampleFilter=require('../filter.js');
const array=[1,2,3,4,5,5];
const resultArray=sampleFilter(array,(element) => {
    return element>4;
});
console.log(`The required array after filtering the original array ${array} is: ${resultArray}`);
